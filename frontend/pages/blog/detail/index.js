import React from "react";
import { createMuiTheme, responsiveFontSizes } from "@material-ui/core/styles";

import { Container, Breadcrumbs, Link, Grid } from "@material-ui/core";
import { NavigateNext } from "@material-ui/icons";
import styles from "./Styles.module.scss";

const Detail = () => {
  let theme = createMuiTheme();
  theme = responsiveFontSizes(theme);

  return (
    <Container className={styles.container} fixed>
      <Grid container spacing={3}>
        <Grid item className={styles.px15} xs={12} md={12}>
          <Breadcrumbs
            separator={<NavigateNext fontSize="small" color="error" />}
            aria-label="breadcrumb"
          >
            <Link color="textPrimary" href="/" fontSize="12px">
              BLOG
            </Link>
          </Breadcrumbs>

          <div className={styles.textTitle}>
            <h3>14 Must-See Bread Making Sites </h3>
          </div>

          <div className={styles.textContent}>
            <h3>
              Last week, we took a look into the book shelf to find our favorite
              bread making books. Big thanks for everyone who shared their own
              suggestions! I was reminded of many great books I haven’t read in
              a while as well as found new books to check out next. This week,
              in that same spirit of sharing bread making resources, I thought
              it would be fun to look at the internet next. So, today, here is
              my list of web sites I return to as often as possible, looking for
              inspiration and ideas, or just to have a good time around bread.
              As with books last week, I’m sure the list is far from complete
              and so I invite your contributions in the comments: if your
              favorite web site (it could even be your own site) isn’t listed,
              leave a comment and tell us why you like that site and why it
              should be included in our list of resources.
            </h3>
          </div>
          <div className={styles.img}>
            <img src="https://images.squarespace-cdn.com/content/v1/56ea2b1b044262c09440118d/1467394057142-0K1DQHLJBF73ZI0TFAZV/ke17ZwdGBToddI8pDm48kDEDYh4Y0JGhR6hzuwcJ44gUqsxRUqqbr1mOJYKfIPR7ObdNX8ufmDOf25UtiTAzdbp5QaNWlTZiIJrq5EjDRI9CRW4BPu10St3TBAUQYVKcC0qNkc-olp59_NUhlNOLqp1WLaCMPdfrfxIgayxkERYosQra53osi0AwhV4aHOLB/image-asset.jpeg?format=750w" />
          </div>
          <div className={styles.textContent}>
            <h3>
              Breadtopia: Breadtopia is a bread making blog and a collection of
              bread making tutorials in both video and text. As the site has
              been around for a long time, it’s archives are a valuable
              resources for anyone looking to learn more about bread making.
            </h3>
          </div>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Detail;

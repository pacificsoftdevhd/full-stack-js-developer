import React from "react";
import { createMuiTheme, responsiveFontSizes } from "@material-ui/core/styles";

import {
  Container,
  Breadcrumbs,
  Link,
  Typography,
  Grid,
  List,
  ListItem,
  ListItemText,
  ListItemAvatar,
  Button,
} from "@material-ui/core";
import {
  NavigateNext,
  Schedule,
  GroupWorkOutlined,
  Print,
  Add,
} from "@material-ui/icons";
import styles from "./Styles.module.scss";

const Detail = () => {
  return (
    <Container className={styles.container} fixed>
      <Grid container spacing={3}>
        <Grid item className={styles.px15} xs={12} md={6}>
          <Breadcrumbs
            separator={<NavigateNext fontSize="small" color="error" />}
            aria-label="breadcrumb"
          >
            <Link color="textPrimary" href="/" fontSize="12px">
              RECIPES
            </Link>
            <Link color="textPrimary" href="/">
              BREAD
            </Link>
            <Typography color="textPrimary">QUICK BREAD</Typography>
          </Breadcrumbs>

          <div className={styles.textTitle}>
            <h3>Whole-Grain Banana Bread </h3>
          </div>

          <div className={styles.textContent}>
            <h3>
              This one-bowl banana -- our 2018 Recipe of the year -- uses the
              simplest ingredients, but is incredibly moist and flavorful. While
              the recipe calls for a 50/50 mix ef flours (all-purpose and whole
              wheat), we often make the bead 100% whole wheat, and honestly? No
              one can tell, it's that good! And not only is this bread delicious
              -- it's versatile.
            </h3>
          </div>

          <div>
            <List>
              <ListItem>
                <ListItemAvatar>
                  <Schedule fontSize="large" />
                </ListItemAvatar>
                <ListItemText primary="Prep" secondary="10 mins" />
                <ListItemText primary="Bake" secondary="1 hr to 1 hr 15 mins" />
                <ListItemText primary="Total" secondary="1 hr 10 mins" />
              </ListItem>
            </List>
          </div>
          <hr />
          <div>
            <List>
              <ListItem>
                <ListItemAvatar>
                  <GroupWorkOutlined fontSize="large" />
                </ListItemAvatar>
                <ListItemText
                  primary="Yield"
                  secondary="1 loaf, 12 generous servings"
                />

                <Button variant="outlined" color="secondary">
                  <Add /> SAVE RECIPE
                </Button>
                <Button variant="outlined" color="secondary">
                  <Print /> PRINT
                </Button>
              </ListItem>
            </List>
          </div>
        </Grid>

        <Grid item className={styles.px15} xs={12} md={6}>
          <img
            src="https://www.kingarthurbaking.com/sites/default/files/styles/featured_image/public/recipe_legacy/4527-3-large.jpg?itok=x587SFUD"
            className={styles.img}
          />
        </Grid>
      </Grid>
    </Container>
  );
};

export default Detail;

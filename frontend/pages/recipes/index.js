import React from "react";
import MenuTop from "../../src/components/menu";
import Tag from "./tag";
import Detail from "./detail";

const Recipes = () => {
  return (
    <>
      <MenuTop />
      <Tag />
      <Detail />
    </>
  );
};

export default Recipes;

import React from "react";
import { Container, Button } from "@material-ui/core/";
import styles from "./Styles.module.scss";

const HeaderTop = () => {
  const menu = ["categories", "learn", "resources"];

  return (
    <div className={styles.topTag}>
      <Container className={styles.container} fixed>
        {menu && menu.map((item, key) => <Button key={key}>{item}</Button>)}
      </Container>
    </div>
  );
};

export default HeaderTop;

import React from "react";
import { createMuiTheme, responsiveFontSizes } from "@material-ui/core/styles";

import {
  Container,
  Breadcrumbs,
  Link,
  Typography,
  Grid,
} from "@material-ui/core";
import { NavigateNext } from "@material-ui/icons";
import styles from "./Styles.module.scss";

const Detail = () => {
  let theme = createMuiTheme();
  theme = responsiveFontSizes(theme);

  return (
    <Container className={styles.container} fixed>
      <Grid container spacing={3}>
        <Grid item className={styles.px15} xs={12} md={12}>
          <Breadcrumbs
            separator={<NavigateNext fontSize="small" color="error" />}
            aria-label="breadcrumb"
          >
            <Link color="textPrimary" href="/" fontSize="12px">
              CREAM
            </Link>
            <Link color="textPrimary" href="/">
              BREAD
            </Link>
            <Typography color="textPrimary">LEARN MAKE BREAD</Typography>
          </Breadcrumbs>

          <div className={styles.textTitle}>
            <h3>Bake bread that makes a difference</h3>
          </div>

          <div className={styles.textContent}>
            <h3>
              Bread is simple in theory, but anyone who has ever tried it will
              understand the complications that come with. Brioche is a
              challenge for beginners, sourdough is a serious situation for
              intermediate bakers, ciabatta can be convoluted even for the
              advanced — as a novice baking bread, you’re entering uncharted
              waters.
            </h3>
          </div>
          <div className={styles.img}>
            <img src="https://cdn.lifestyleasia.com/wp-content/uploads/sites/2/2020/04/09125745/jonathan-pielmayer-j1gr2w10EtQ-unsplash-1024x683.jpg" />
          </div>
          <div className={styles.textContent}>
            <h3>
              Do not let that deter you, though. Learning how to make bread is a
              life skill worth investing in. It also doubles up as a way to pass
              time, and a fun activity to keep your household from
              disintegrating after jigsaw puzzles stop cutting it. To help you,
              we’ve rounded up some of our favourite resources to learn how to
              make bread on the internet, from blogs to highly rated recipes.
            </h3>
          </div>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Detail;

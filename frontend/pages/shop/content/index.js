import React from "react";
import ShopCard from "../card";

import { Container } from "@material-ui/core";

import styles from "./Styles.module.scss";

const Content = () => {
  return (
    <Container fixed>
      <div className={styles.content}>
        <ShopCard />
        <ShopCard />
        <ShopCard />
        <ShopCard />
        <ShopCard />
        <ShopCard />
      </div>
    </Container>
  );
};

export default Content;

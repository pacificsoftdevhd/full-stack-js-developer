import React from "react";
import MenuTop from "../../src/components/menu";
import ShopContent from "./content";
import Tag from "./tag";

const shop = () => {
  return (
    <>
      <MenuTop />
      <Tag />
      <ShopContent />
    </>
  );
};

export default shop;

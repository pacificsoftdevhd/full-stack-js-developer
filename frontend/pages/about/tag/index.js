import React from "react";
import { useRouter } from "next/router";
import { makeStyles } from "@material-ui/core/styles";
import { Container, Button } from "@material-ui/core/";
import styles from "./Styles.module.scss";

const HeaderTop = () => {
  const menu = ["categories", "about", "resources"];

  return (
    <div className={styles.topTag}>
      <Container className={styles.container} fixed>
        {menu && menu.map((item, key) => <Button key={key}>{item}</Button>)}
      </Container>
    </div>
  );
};

export default HeaderTop;

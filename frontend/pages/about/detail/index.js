import React from "react";
import { useRouter } from "next/router";
import {
  createMuiTheme,
  responsiveFontSizes,
  ThemeProvider,
} from "@material-ui/core/styles";

import {
  Container,
  Breadcrumbs,
  Link,
  Typography,
  Grid,
  List,
  ListItem,
  ListItemText,
  ListItemAvatar,
  Avatar,
  Button,
} from "@material-ui/core";
import {
  NavigateNext,
  Schedule,
  Mode_standby,
  GroupWorkOutlined,
  Print,
  Add,
} from "@material-ui/icons";
import styles from "./Styles.module.scss";

const Detail = () => {
  let theme = createMuiTheme();
  theme = responsiveFontSizes(theme);

  return (
    <Container className={styles.container} fixed>
      <Grid container spacing={3}>
        <Grid item className={styles.px15} xs={12} md={12}>
          <Breadcrumbs
            separator={<NavigateNext fontSize="small" color="error" />}
            aria-label="breadcrumb"
          >
            <Link color="textPrimary" href="/" fontSize="12px">
              ABOUT
            </Link>
          </Breadcrumbs>

          <div className={styles.textTitle}>
            <h3>Bake bread that makes a difference </h3>
          </div>

          <div className={styles.textContent}>
            <h3>
              Bread relates people to each other. It speaks to the heart of our
              relationships, and to our ties with nature, art, history, society,
              and science. At the crossroads of home baking and professional
              craft baking, BREAD Magazine is an instructive, entertaining and
              inspiring journey that fosters meaning and deeper connections.
            </h3>
          </div>
          <div className={styles.img}>
            <img src="http://www.rippleslearning.com/wp-content/uploads/2018/09/about-us-1024x683.jpg" />
          </div>
          <div className={styles.textContent}>
            <h3>
              BREAD is an independent magazine for bread lovers, made by bread
              lovers. It is a magazine for people who love and make great bread
              — for anyone who finds joy and fulfillment in the act of mixing a
              dough, the slow rhythm of natural fermentation, and listening to
              the crackling sound that comes from fresh loaves of bread as they
              cool in the quiet of the home kitchen or the bakery. As the name
              implies, the focus in the magazine is strictly on bread: no cakes,
              no pastry. No food other than bread. If that sounds like the right
              kind of focus for you, maybe you are a bread lover just like we
              are? In this case, BREAD is for you.
            </h3>
          </div>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Detail;

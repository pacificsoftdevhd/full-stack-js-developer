//env
require("./env");

const express = require("express");
const mongoose = require("mongoose");
const config = require("./config/index");
const app = express();
const http = require("http").Server(app);
const port = process.env.PORT || 3000;

require("./config/express")(app);

const io = require("socket.io")(http);

io.on("connection", function (socket) {
  socket.on("news", function (data) {
    io.sockets.emit("news", socket.id + " send all client: " + data);
  });
});

const listen = () => {
  new Promise((rs, rj) => {
    http.listen(port, () => {
      console.log("Server running at port: " + port);
    });
  });
};

listen();

process.on("uncaughtException", (err) => {
  console.log("uncaughtException server.js ERROR: ", err);
});

process.on("unhandledRejection", (err) => {
  console.log(err.message);
});

module.exports = app;

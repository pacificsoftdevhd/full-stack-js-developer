const modifyResponse = require('./modifyResponse');
const auth = require('./auth');
module.exports = {
    auth,
    modifyResponse,
}
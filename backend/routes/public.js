const router = require("express").Router();
const { Controller } = require("../app/modules/findServers");

router.get("/findServers", Controller.findServer);

module.exports = router;

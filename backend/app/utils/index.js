const constants = require("./constants");
const servers = require("./jsonServers");
const dynamicSort = require("./dynamicSort");

module.exports = {
    constants,
    servers,
    dynamicSort
}
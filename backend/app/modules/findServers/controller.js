const { servers, dynamicSort } = require("../../utils/index");
const linkCheck = require("link-check");
const constants = require("../../utils/constants");

const findServer = (req, res) => {
  let promises = []
  servers.forEach(item => {
    let promise = new Promise((resolve, reject) => {
      return linkCheck(item.url, { timeout: "2500" }, function (err, result) {
        if (err) {
          console.error(err);
          reject(result)
        } else {
          resolve({
            url: result.link,
            priority: item.priority,
            status: result.status
          })
        }
      })
    });
    promises.push(promise)
  })
  Promise.all(promises).then(data => {
    data = data.filter((item) => item.status === "alive").sort(dynamicSort("priority"));
    if (!data)
      return res
        .status(constants.CODE.BAD_REQUEST)
        .send("All servers are dead ");
    else
      return res
        .status(constants.CODE.GET_OK)
        .send(`Server ${data[0].url} is alive`);
  })
}

module.exports = {
  findServer,
};

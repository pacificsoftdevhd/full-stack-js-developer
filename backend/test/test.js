let chai = require("chai");
let chaiHttp = require("chai-http");
let server = require("../server");
let expect = chai.expect;
let should = chai.should();
chai.use(chaiHttp);
describe("Fullstack App", () => {
  describe(" Check server ", () => {
    it("Can call the findServer function correctly  ", (done) => {
      chai
        .request(server)
        .get("/api/pl/findServers")
        .end((err, res) => {
          expect(res.text).to.equal("Server http://app.scnt.me is alive");
          done();
        });
    });
  });
});

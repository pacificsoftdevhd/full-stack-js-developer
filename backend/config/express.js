const express = require("express");
const apiRouterPL = require("../routes/public");
const bodyParser = require("body-parser");
const { modifyResponse, auth } = require("../middleware");
const cors = require("cors");

module.exports = (app) => {
  app.use(
    bodyParser.urlencoded({
      extended: true,
    })
  );

  app.use(cors());

  app.use(modifyResponse);

  app.use(bodyParser.json());

  app.get("", function (req, res) {
    return res.send(
      `<a href="http://localhost:3001/api/pl/findServers">Check function findServers</a>`
    );
  });
  app.use("/api/pl", apiRouterPL);
};
